const {app, BrowserWindow, dialog, Menu } = require('electron');
const path = require('path')
const fs = require('fs')

let mainWindow;

app.on('window-all-closed', function() {
  app.quit();
});

app.on('ready', function() {
  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu)



  mainWindow = new BrowserWindow({x: 0, y: 0, width: 1024, height: 700 }); //, frame: false });
  mainWindow.loadURL('file://' + __dirname + '/index.html');
  require('devtron').install()
  

// tak jak document.onReady in mainProcess
  // mainWindow.webContents.on('did-finish-load', () => {
  //   // openFile()
  // })
});

function openFile() {
  // const {dialog} = require('electron')
  // console.log(dialog.showOpenDialog({properties: ['openFile', 'openDirectory', 'multiSelections']}))

  const files = dialog.showOpenDialog(mainWindow, {
    properties: ['openfile'],
    filters: [{
      name: 'Markdown files', extensions: ['md', ,'txt']
    }],
    buttonLabel: 'Właź',
    title: 'Otwieramy',
    message: 'mess'
  })

  if(!files) return

  const file = files[0]

  const content = fs.readFileSync(file).toString()
  // console.log(content)
  mainWindow.webContents.send('file-opened', file, content)
  mainWindow.setTitle(file)// __dirname;
}

function saveFile(content) {
  const filename = dialog.showSaveDialog(mainWindow, {
    title: ' Save Html output',
    filters: [
      { name: 'HTML Files', extensions: ['txt', 'md', 'html']}
    ]
  })

  if(!filename) return

  fs.writeFileSync(filename, content)
}

// in order to import it in app.js
exports.openFile = openFile;
exports.saveFile = saveFile;




const template = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Open...',
        accelerator: 'CmdOrCtrl+O',
        click () { openFile() }
      },
      {
        label: 'Save...',
        accelerator: 'CmdOrCtrl+S', // bo w Macu jest Ctrl
        click () {
          // We can't call saveFile(content) directly because we need to get
          // the content from the renderer process. So, send a message to the
          // renderer, telling it we want to save the file.
          mainWindow.webContents.send('save-file')
        }
      }
    ]
  },
  {
    label: 'Edit',
    submenu: [
      {
        label: 'Undo',
        accelerator: 'CmdOrCtrl+Z',
        role: 'undo'
      },
      {
        label: 'Redo',
        accelerator: 'Shift+CmdOrCtrl+Z',
        role: 'redo'
      },
      {
        type: 'separator'
      },
      {
        label: 'Cut',
        accelerator: 'CmdOrCtrl+X',
        role: 'cut'
      },
      {
        label: 'Copy',
        accelerator: 'CmdOrCtrl+C',
        role: 'copy'
      },
      {
        label: 'Paste',
        accelerator: 'CmdOrCtrl+V',
        role: 'paste'
      },
      {
        label: 'Select All',
        accelerator: 'CmdOrCtrl+A',
        role: 'selectall'
      }
    ]
  },
  {
    label: 'Developer',
    submenu: [
      {
        label: 'Toggle Developer Tools',
        accelerator: process.platform === 'darwin'
          ? 'Alt+Command+I'
          : 'Ctrl+Shift+I',
        click () { mainWindow.webContents.toggleDevTools() }
      }
    ]
  }
]

if (process.platform === 'darwin') {
  const name = app.getName()
  template.unshift({
    label: name,
    submenu: [
      {
        label: 'About ' + name,
        role: 'about'
      },
      {
        type: 'separator'
      },
      {
        label: 'Services',
        role: 'services',
        submenu: []
      },
      {
        type: 'separator'
      },
      {
        label: 'Hide ' + name,
        accelerator: 'Command+H',
        role: 'hide'
      },
      {
        label: 'Hide Others',
        accelerator: 'Command+Alt+H',
        role: 'hideothers'
      },
      {
        label: 'Show All',
        role: 'unhide'
      },
      {
        type: 'separator'
      },
      {
        label: 'Quit',
        accelerator: 'Command+Q',
        click () { app.quit() }
      }
    ]
  })
}