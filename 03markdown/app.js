const electron = require('electron')
const shell = electron.shell
const ipc = electron.ipcRenderer // nasluchuje eventy z innych procesow
const marked = require('marked')

// tunel do innych procesów
const remote = electron.remote
const clipboard = remote.clipboard
// index.js w ramach mainProcessa
const mainProcess = remote.require('./index.js')

const $ = selector => document.querySelector(selector)
// mainWindow.webContents.openDevTools()

const rawMarkdown = $('.raw-markdown')
const renderedHtml = $('.rendered-html')
const openFile = $('#open-file')
const showFolder = $('#show-folder')
// copy-html 
const copyHtml = $('#copy-html')
// save-file
const saveFile = $('#save-file')

ipc.on('file-opened', (event, file, content) => {
    console.log(content)
    rawMarkdown.value = content
    renderMarkdownToHtml(content)
})

function renderMarkdownToHtml(markdown) {
    const html = marked(markdown)
    renderedHtml.innerHTML = html;
}

rawMarkdown.addEventListener('keyup', (event) => {
    const content = event.target.value
    renderMarkdownToHtml(content)
})

openFile.addEventListener('click', () => {
    mainProcess.openFile()
})

copyHtml.addEventListener('click', () => {
    const html = renderedHtml.innerHTML
    
    clipboard.writeText(html)
})

saveFile.addEventListener('click', () => {
    const md = rawMarkdown.value
    mainProcess.saveFile(md)
    // clipboard.writeText(html)
    console.log("saved")
})

document.body.addEventListener('click', (event) => {
    if(event.target.matches('a[href^="http"]')){
        event.preventDefault()
        shell.openExternal(event.target.href)

        
    }

})

showFolder.addEventListener('click', () => {
    if(event.target.matches('a[href^="http"]')){
        event.preventDefault()
        shell.showItemInFolder()

    }
})
